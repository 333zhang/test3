DOCKER_NAME ?= rcore-init
.PHONY: docker build_docker

docker:
	docker run --rm -it --mount type=bind,source=$(shell pwd),destination=/mnt ${DOCKER_NAME}

build_docker: 
	docker build -t ${DOCKER_NAME} .

SUBDIR=./os
MAKE=make
all:
